const mongoose = require('mongoose')

const Schema = mongoose.Schema

const noteSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        require: true
    },
    color: {
        type: Number,
    }
}, { timestamps: true })

module.exports = mongoose.model('Note', noteSchema)