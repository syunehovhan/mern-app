import { useNotesContext } from '../hooks/useNotesContext'
import DeleteIcon from '../icons/Delete'
import Moment from 'moment';

// date fns
// import formatDistanceToNow from 'date-fns/formatDistanceToNow'

const NoteListItem = ({ note, setId }) => {
  const { dispatch } = useNotesContext()

  const handleDelete = async () => {

    const response = await fetch('api/notes/' + note._id, {
      method: 'DELETE'
    })
    const json= await response.json()

    if (response.ok) {
      dispatch({type: 'DELETE_NOTE', payload: json})
    }
  }

  return (
    <div className={`notes-list-item color-${note.color}`}>
        <h4 onClick={() => setId(note._id)}>{note.title}</h4>
        <p>{note.content}</p>
        <span>{Moment(note.createdAt).format('DD/MM/YYYY HH:mm')}</span>
        <DeleteIcon onClick={handleDelete}/>
    </div>
  )
}

export default NoteListItem