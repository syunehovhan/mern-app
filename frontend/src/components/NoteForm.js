import { useState } from 'react'
import { useNotesContext } from '../hooks/useNotesContext'

const NoteForm = ({setLoading}) => {
  const { dispatch } = useNotesContext()

  const [title, setTitle] = useState('')
  const [content, setContent] = useState('')
  const [color, setColor] = useState(0)
  const [error, setError] = useState(null)
  const [emptyFields, setEmptyFields] = useState([])

  const colors = [
    {index: 0, name: 'gray', code: '#777777'},
    {index: 1, name: 'pink', code: '#F88379'},
    {index: 2, name: 'blue', code: '#ADD8E6'},
    {index: 3, name: 'green', code: '#93C572'},
    {index: 4, name: 'orange', code: '#F28C28'},
    {index: 5, name: 'yellow', code: '#FFD700'},
    {index: 6, name: 'red', code: '#FF5733'},
  ]

  const handleSubmit = async (e) => {
    e.preventDefault()
    setLoading(true)
    
    const note = {title, content, color}

    try {
      
      const response = await fetch('/api/notes', {
        method: 'POST',
        body: JSON.stringify(note),
        headers: {
          'Content-Type': 'application/json'
        }
      })
      
      const json = await response.json()
      
      if (!response.ok) {
        setError(json.error)
        setEmptyFields(json.emptyFields)
      } else {
        setEmptyFields([])
        setError(null)
        setTitle('')
        setColor(0)
        setContent('')
        dispatch({type: 'CREATE_NOTE', payload: json})
      }
      
    } finally {
      setLoading(false)
    }
  } 

  return (
    <form className="create" onSubmit={handleSubmit}> 
      <h3>Add a New Note</h3>

      <label>Excersize Title:</label>
      <input 
        type="text" 
        onChange={(e) => setTitle(e.target.value)} 
        value={title}
        className={emptyFields.includes('title') ? 'error' : ''}
      />

      <label>Content:</label>
      <textarea 
        type="number" 
        rows={8}
        onChange={(e) => setContent(e.target.value)}
        className={emptyFields.includes('content') ? 'error' : ''}
        value={content}
      ></textarea>

      <label>Color:</label>
      <select 
        onChange={(e) => setColor(e.target.value)}
        className={emptyFields.includes('color') ? 'error' : ''}
        
      >
        {colors.map((val) => (
          <option selected={val.index == color} value={val.index}>{val.name}</option>
        ))}
      </select>

      <button>Add Note</button>
      {error && <div className="error">{error}</div>}
    </form>
  )
}

export default NoteForm