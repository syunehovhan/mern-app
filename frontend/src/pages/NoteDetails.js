import { useEffect, useState } from 'react';
import { useNotesContext } from '../hooks/useNotesContext'
import DeleteIcon from '../icons/Delete'
import Moment from 'moment';
import { useParams } from 'react-router-dom';

const NoteDetails = () => {
  const { dispatch } = useNotesContext()
  const [note, setNote] = useState({})
  const [disableButton, setDisableButton] = useState(false)
  const { id } = useParams()

  useEffect(() => {
    const fetchNote = async () => {
      const response = await fetch('api/notes/' + id)
      const json= await response.json()
  
      if (response.ok) {
        setNote(json)
      }
    }
    fetchNote()
  }, [])

  useEffect(() => {
    setDisableButton(false)
  }, [note])

  const handleClick = async () => {

    const data = {
      title: note.title,
      content: note.content,
      color: note.color
    }
    const response = await fetch('api/notes/' + id, {
      method: 'PATCH',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    })
    const json = await response.json()

    if (response.ok) {
      setDisableButton(true)
    } else {
      alert('Something went wrong!')
    }
  }

  return ( // sue > to do: add a color picker
    <div className={`note-details color-${note.color}`}>
      <input 
        type="text" 
        onChange={(e) => setNote((prev) => {return {...prev, title: e.target.value}})}
        value={note.title}
      />
      <textarea 
        type="number" 
        rows={8}
        onChange={(e) => setNote((prev) => {return {...prev, content: e.target.value}})}
        value={note.content}
      ></textarea>
      <span>{Moment(note.createdAt).format('DD/MM/YYYY HH:mm')}</span>
      {disableButton && <div className='saved'>saved ✓</div>}
      <button disabled={disableButton} onClick={handleClick}>Save Changes</button>
    </div>
  )
}

export default NoteDetails