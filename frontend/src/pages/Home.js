import { useEffect, useState } from "react"
import { useNotesContext } from "../hooks/useNotesContext"

// components
import NoteListItem from "../components/NoteListItem"
import NoteForm from "../components/NoteForm"
import Loader from "../components/Loader"
import NoteDetails from "./NoteDetails"
import { useNavigate } from "react-router-dom"
// import { NotesContext } from "../context/NotesContext"

const Home = () => {
  const navigate = useNavigate()

  const {notes, dispatch} = useNotesContext()
  const [loading, setLoading] = useState(false)
  const [id, setId] = useState(null)

  useEffect(() => {
    if (id) {
      navigate(`/${id}`)
    }
  }, [id])

  useEffect(() => {
    const fetchNotes = async () => {
      try {
        setLoading(true)
        const response = await fetch('/api/notes')
        const json = await response.json()
        
        if (response.ok) {
          dispatch({ type: 'SET_NOTES', payload: json })
        }
      } finally {
        setLoading(false)
      }
    }

    fetchNotes()
  }, [])

  return (
    <div className="home">
      <div className="notes">
        {loading ? <Loader/> : (
          <>
            {notes && notes.map((note) => (
              <NoteListItem key={note._id} note={note} setId={setId}/>
            ))}
          </>
        )}
      </div>
      <NoteForm setLoading={setLoading} />
    </div>  
  )
}

export default Home